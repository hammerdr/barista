#!/usr/bin/env objj
require("narwhal").ensureEngine("rhino");

@import <Foundation/Foundation.j>
@import "context.j";
CPLogRegister(CPLogPrint, "info");
FILE = require("file");
SYSTEM = require("system");
OS = require("OS");

INFO = "\n"+
"Barista\n"+
"This is a tool for generating and running barista tests\n"+
"\n"+
"\tinfo\t\tThis\n"+
"\tinit\t\tWill initialize Barista in an existing Cappuccino project\n"+
"\trun\t\tWill run Barista tests in your default browser\n"+
"\tgenerate\tWill generate scripts and steps\n"+
"\t  script {name}\tGenerates a script by the {name} you give it\n"+
"\t  when {name}\tGenerates a when step by the {name} you give it\n"+
"\t  then {name}\tGenerates a then step by the {name} you give it\n"+
"\t  given {name}\tGenerates a given step by the {name} you give it\n";

@implementation Barista : CPObject

+ (void)handleArguments:(CPArray)args
{
	if(args[1] == "init")
	{
		if(true || FILE.isFile("AppController.j"))
		{
			CPLog.info(["Installing framework.."], "info", "Barista");
			try
			{
				var baristaFramework = FILE.join(SYSTEM.prefix, "packages", "barista", "Framework", "Barista");
				var target = FILE.join("Frameworks", "Barista");
				
				
				if (FILE.isDirectory(target)) {
					CPLog.info("Removing old Framework installation..");
				    FILE.rmtree(target);
				}
				
				FILE.copyTree(baristaFramework, FILE.cwd() + "/Frameworks/Barista");
			}
			catch(ex)
			{
				CPLog.error("Error installing frameworks! Exiting..");
				return;
			}
			
			CPLog.info("Installing libraries..");
			try
			{
				var baristaLibraryScripts = FILE.join(SYSTEM.prefix, "packages", "barista", "Resources", "Scripts");
				var targetScripts = FILE.join("Test", "Barista", "Scripts");
				
				var baristaLibraryStepDefinitions = FILE.join(SYSTEM.prefix, "packages", "barista", "Resources", "StepDefinitions");
				var targetStepDefinitions = FILE.join("Test", "Barista", "StepDefinitions");
				
				var baristaLibraryIndex = FILE.join(SYSTEM.prefix, "packages", "barista", "Resources", "index-barista.html");
				var targetIndex = FILE.join("index-barista.html");
				
				var testsFolder = FILE.join("Test");
				
				if(!FILE.isDirectory(testsFolder))
				{
					FILE.mkdir(testsFolder);
				}
				
				var baristaFolder = FILE.join("Test", "Barista");

				if(!FILE.isDirectory(baristaFolder))
				{
					FILE.mkdir(baristaFolder);
				}
				
				if(FILE.isDirectory(targetScripts))
				{
					CPLog.info("Scripts already exist.. barista won't remove these. You'll have to do it manually and rerun this.");
				}
				else
				{
					FILE.copyTree(baristaLibraryScripts, targetScripts + "/");
				}

				if(FILE.isDirectory(targetStepDefinitions))
				{
					CPLog.info("StepDefinitions already exist.. barista won't remove these. You'll have to do it manually and rerun this.");
				}
				else
				{
					FILE.copyTree(baristaLibraryStepDefinitions, targetStepDefinitions + "/");
				}
				
				if(FILE.isFile(targetIndex))
				{
					CPLog.info("Removing old index-barista file.");
					FILE.rmtree(targetIndex);
				}
				FILE.copy(baristaLibraryIndex, targetIndex);
			}
			catch(ex)
			{
				CPLog.error("Error installing libraries! Exiting..");
				return;
			}
			
			CPLog.info("Barista installed successfully. To run your tests, run \"barista run\"");
		}
		else
		{
			CPLog.error("You need to be in the toplevel folder of a Cappuccino application!");
		}
	}
	else if(args[1] == "unit")
	{
        var unitCommand = FILE.join(SYSTEM.prefix, "packages", "barista", "Framework", "Barista", "Unit", "UIUnitTestRunner.j");
	    var filePath = FILE.path(args[2]);

        var context = new ObjJContext(filePath);
        
        var _OBJJ = context.require("objective-j");
        
        context.setIncludePaths(['Frameworks']);
        context.setEnvironments(["Browser", "ObjJ"]);
        context.initializeGlobalRecorder();

        _OBJJ.objj_eval(
            "("+(function(path) {
                fileImporterForPath(".")(path, true, function() {});
            })+")"
        )(unitCommand);
        
        _OBJJ.window.main(filePath.absolute());
	}
	else if(args[1] == "run")
	{
		OS.command("open index-barista.html");
	}
	else if(args[1] == "generate")
	{
	    switch(args[2])
	    {
	        case "given":
	            FILE.copy(FILE.join(SYSTEM.prefix, "packages", "barista", "Resources", "Templates", "GivenTemplate.j"), 
	                "Test/Barista/StepDefinitions/"+ args[3]+".j");
	        
	            break;
	            
	        case "then":
    	        FILE.copy(FILE.join(SYSTEM.prefix, "packages", "barista", "Resources", "Templates", "ThenTemplate.j"), 
    	            "Test/Barista/StepDefinitions/" + args[3]+".j");
	        
	            break;
	            
	        case "when":
    	        FILE.copy(FILE.join(SYSTEM.prefix, "packages", "barista", "Resources", "Templates", "WhenTemplate.j"), 
    	            "Test/Barista/StepDefinitions/" + args[3]+".j");
	        
	            break;
	            
	        case "script":
	            FILE.copy(FILE.join(SYSTEM.prefix, "packages", "barista", "Resources", "Templates", "ScriptTemplate.script"), 
	                "Test/Barista/Scripts/"+args[3]+".feature");
	                
	            objj_importFile(FILE.absolute(FILE.join("Test", "Barista", "Scripts", "ScriptList.js")), YES, function() {
    	            SCRIPT_LIST.push(args[3]+".feature");

    	            FILE.remove(FILE.join("Test", "Barista", "Scripts", "ScriptList.js"));
    	            FILE.write(FILE.join("Test", "Barista", "Scripts", "ScriptList.js"), "SCRIPT_LIST = " + JSON.stringify(SCRIPT_LIST) + ";", "w");
	            });
	                
	            break;
	            
	        default:
	            CPLog.info("Can't generate " + args[2]);
	            break;
	    }
	}
	else
	{
		[self info];
	}
}

+ (void)info
{
	CPLog.info(INFO);
}

@end

function main(args) {
    // This was removed by the new loader. I WANT THIS BACK! RAWR!
    // CPLog["error"] = function(msg){_CPLogDispatch([msg], "error", "Barista")};
    // CPLog["info"] = function(msg){_CPLogDispatch([msg], "info", "Barista")};
    // CPLog["warn"] = function(msg){_CPLogDispatch([msg], "warn", "Barista")};

    [Barista handleArguments:args];
}
