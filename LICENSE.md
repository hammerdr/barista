Released under the MIT License. http://www.opensource.org/licenses/mit-license.php

Copyright 2009, Derek Hammer.

Built using Cappuccino. http://cappuccino.org

Images in the Framework/Barista/Reporting/Resources folder released under GPL by WooThemes, Inc.