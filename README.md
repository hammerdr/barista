Barista development has stopped. All of my work and effort is now going into [CuCapp](http://github.com/hammerdr/cucapp). It's pretty awesome. You should take a gander.

License
-------

Barista is released under the MIT License. For more information please refer to LICENSE.md

Contact
-------

If you have questions, I'd be happy to help.

Twitter: @hammerdr  
derek.r.hammer@gmail.com  
Cappuccino IRC (irc.freenode.com#cappuccino)  
Cappuccino Mailing List  