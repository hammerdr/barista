/*
 * AppController.j
 * UITesting
 *
 * Created by You on November 20, 2009.
 * Copyright 2009, Your Company All rights reserved.
 */

@import <Foundation/Foundation.j>
@import <AppKit/AppKit.j>
@import <Barista/UITestRunner.j>

@import "../../../AppController.j"

function main(args, namedArgs)
{
	[UITestRunner go];
}
