@import "StepDefinitions/StepDefinitions.j"
@import "../../Tests/Barista/StepDefinitions/StepDefinitions.j"

@implementation UITestScriptParser : CPObject

+ (CPArray)parse:(CPString)fileData withDelegate:(UITestScript)delegate
{
	var context = [CPDictionary dictionary];
	var data = fileData.split("\n");
	var loadedData = [CPArray array];
	
	for(var i = 0; i < [data count]; i++)
	{
		if([data objectAtIndex:i].match(@"^#") || [data objectAtIndex:i].match(@"^\/") 
			|| [data objectAtIndex:i].match(/^Comment/) || [data objectAtIndex:i].match(/^\s*$/))
		{
			continue;
		}
		
		[loadedData addObject:[UITestScriptParser parseLine:[data objectAtIndex:i] withContext:context delegate:delegate]];
	}
	
	return loadedData;
}

+ (Function)parseLine:(CPString)lineData withContext:(CPDictionary)context delegate:(UITestScript)delegate
{
	try
	{
		if(lineData.match(/Feature:.*/))
		{
			return parseFeature(lineData, context, delegate);
		}
		else if(lineData.match(/In order.*/))
		{
			return function() {};
		}
		else if(lineData.match(/As.*/))
		{
			return function() {};
		}
		else if(lineData.match(/I.*/))
		{
			return function() {};
		}
		else if(lineData.match(/Scenario:.*/))
		{
			return parseScenario(lineData, context, delegate);
		}
		else if(lineData.match(/Given.*/))
		{
			return parseGivenExpression(lineData, context, delegate);
		}
		else if(lineData.match(/When.*/))
		{
			return parseWhenExpression(lineData, context, delegate);
		}
		else if(lineData.match(/Then.*/))
		{
			return parseThenExpression(lineData, context, delegate);
		}
		else if(lineData.match(/And.*/))
		{
			return parseAndExpression(lineData, context, delegate);
		}
		else
		{
			return parseError(lineData, context, delegate);
		}
	}
	catch(ex)
	{
		return parseError(lineData, context, delegate);
	}
}

@end

function parseError(lineData, context, delegate)
{
	return function()
	{
		[delegate addError:"Was unable to parse the line " + lineData];
	}
}

function parseFeature(lineData, context, delegate)
{
	return function()
	{
		[delegate setCurrentFeature:lineData.match(/Feature:\s*(.*)/)[1]];
	}
}

function parseScenario(lineData, context, delegate)
{
	return function()
	{
		[delegate setCurrentScenario:lineData.match(/Scenario:\s*(.*)/)[1]];
	}
}

function parseGivenExpression(lineData, context, delegate)
{
	if(lineData.match(/Given\s*(.*)/))
	{
		return function()
		{
			[context setObject:"given" forKey:@"lastExpression"];
			var argument = lineData.match(/Given\s*(.*)/)[1];
			var found = false;
			for(var i = 0; i < [USER_GIVEN_DEFINITIONS count]; i++)
			{
				if([[USER_GIVEN_DEFINITIONS objectAtIndex:i] matches:argument])
				{
					found = true;
					[[USER_GIVEN_DEFINITIONS objectAtIndex:i] parse:argument context:context delegate:delegate];
					break;
				}
			}
			if(!found)
			{
				for(var i = 0; i < [GIVEN_DEFINITIONS count]; i++)
				{
					if([[GIVEN_DEFINITIONS objectAtIndex:i] matches:argument])
					{
						[[GIVEN_DEFINITIONS objectAtIndex:i] parse:argument context:context delegate:delegate];
						break;
					}
				}
			}
		};
	}
	else
	{
		return parseError(lineData, context, delegate);
	}
}

function parseWhenExpression(lineData, context, delegate)
{
	if(lineData.match(/When\s*(.*)/))
	{
		return function()
		{
			[context setObject:"when" forKey:@"lastExpression"];
			var argument = lineData.match(/When\s*(.*)/)[1];
			var found = false;
			for(var i = 0; i < [USER_WHEN_DEFINITIONS count]; i++)
			{
				if([[USER_WHEN_DEFINITIONS objectAtIndex:i] matches:argument])
				{
					found = true;
					[[USER_WHEN_DEFINITIONS objectAtIndex:i] parse:argument context:context delegate:delegate];
					break;
				}
			}
			if(!found)
			{
				for(var i = 0; i < [WHEN_DEFINITIONS count]; i++)
				{
					if([[WHEN_DEFINITIONS objectAtIndex:i] matches:argument])
					{
						[[WHEN_DEFINITIONS objectAtIndex:i] parse:argument context:context delegate:delegate];
						break;
					}
				}
			}
		};
	}
	else
	{
		return parseError(lineData, context, delegate);
	}
}

function parseThenExpression(lineData, context, delegate)
{
	if(lineData.match(/Then\s*(.*)/))
	{
		return function()
		{
			[context setObject:"then" forKey:@"lastExpression"];
			var argument = lineData.match(/Then\s*(.*)/)[1];
			var found = false;
			for(var i = 0; i < [USER_THEN_DEFINITIONS count]; i++)
			{
				if([[USER_THEN_DEFINITIONS objectAtIndex:i] matches:argument])
				{
					found = true;
					[[USER_THEN_DEFINITIONS objectAtIndex:i] parse:argument context:context delegate:delegate];
					break;
				}
			}
			if(!found)
			{
				for(var i = 0; i < [THEN_DEFINITIONS count]; i++)
				{
					if([[THEN_DEFINITIONS objectAtIndex:i] matches:argument])
					{
						[[THEN_DEFINITIONS objectAtIndex:i] parse:argument context:context delegate:delegate];
						break;
					}
				}
			}
		};
	}
	else
	{
		return parseError(lineData, context, delegate);
	}
}

function parseAndExpression(lineData, context, delegate)
{
	return function()
	{
		if([context objectForKey:"lastExpression"] == "given")
		{
			parseGivenExpression(lineData, context, delegate)();
		}
		else if([context objectForKey:"lastExpression"] == "when")
		{
			parseWhenExpression(lineData, context, delegate)();
		}
		if([context objectForKey:"lastExpression"] == "then")
		{
			parseThenExpression(lineData, context, delegate)();
		}
	};
}
