@import "UITestCase.j"
@import <Foundation/Foundation.j>
@import <AppKit/AppKit.j>

var stream = require("term").stream;

@implementation UIUnitTestRunner : CPObject
{
    CPArray         files;
    CPNumber        currentFile;
    CPArray         startupWindows;
    CPArray         failures;
    CPArray         tests;
}

+ (void)startWithArguments:(CPArray)arguments
{
    var runner = [[self alloc] initWithFiles:arguments];
    
    [runner start];
}

- (id)initWithFiles:(CPArray)someFiles
{
     if(self = [super init])
     {
         files = someFiles;
         currentFile = 0;
         tests = [CPArray array];
         failures = [CPArray array];
     }
     return self;
}

- (void)start
{
    if(currentFile == [files count])
    {
        if([failures count] == 0)
        {
            stream.print("\0green(All tests passed!\0)");
        }
        else
        {
            stream.print("\0red(There were " + [failures count] + " failures!\0)");
        }
        
        stream.print("Number of tests: " + [tests count]);
        return;
    }

    CPApp = nil;
    [_CPAppBootstrapper reset];
    CPApplicationMain(nil, nil);
    [self run];
}

- (void)run
{
    var folders = files[currentFile].split("/");
    var testClassFileName = folders[folders.length-1];
    var testClassName = testClassFileName.replace(".j", "");
    require(files[currentFile].toString());
    var test = [CPClassFromString(testClassName) run];
    
    [tests addObjectsFromArray:[test tests]];
    [failures addObjectsFromArray:[test failures]];
    
    currentFile = currentFile + 1;
    [self start];
}

@end

function main(args) {
    [UIUnitTestRunner startWithArguments:[args]];
}