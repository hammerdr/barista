@import <Foundation/CPObject.j>

UIViewHorizontalCenterAlignment = 2 << 0;
UIViewHorizontalLeftAlignment = 2 << 1;

@implementation UITestCase : CPObject
{
    CPArray         failures        @accessors(readonly);
    CPArray         tests           @accessors(readonly);
}

+ (UITestCase)run
{
    var testCase = [[self alloc] init];
    var methodList = class_copyMethodList(self);
    
    for(var i = 0; i < [methodList count]; i++)
    {
        if([methodList[i].name hasPrefix:@"test"])
        {
            var aTest = function()
            {
                objj_msgSend(testCase, "setUp");
                objj_msgSend(testCase, methodList[i].name);
                objj_msgSend(testCase, "tearDown");
            };
            
            [testCase runTestCase:aTest testName:methodList[i].name];
        }
    }
    
    return testCase;
}

- (id)init
{
    self = [super init];
    if(self)
    {
        failures = [CPArray array];
        tests = [CPArray array];
    }
    return self;
}

- (void)setUp
{
    
}

- (void)tearDown
{
    
}
    
- (void)runTestCase:(Function)testCase testName:(CPString)name
{
    var aWindow = [self window];
    [[aWindow contentView] addSubview:[self view]];
    
    try{testCase();}
    catch(ex){[self addFailure:name withReason:ex];}
    finally{[self testFinished:name];}
}

- (void)addFailure:(CPString)test withReason:(Exception)exception
{
    print("Test <" + test + "> failed! Reason: " + exception);
    [failures addObject:test];
}

- (void)testFinished:(CPString)test
{
    [tests addObject:test];
    CPApp._windows = [CPArray array];
}

- (CPView)view
{
    return [[CPView alloc] initWithFrame:CGRectMakeZero()];
}

- (CPWindow)window
{
    return [[CPWindow alloc] initWithContentRect:CGRectMakeZero() styleMask:CPBorderlessBridgeWindowMask];
}

- (void)clickMiddleOfControl:(CPControl)aControl
{
    [self clickXPositionPercentage:0.5 yPositionPercentage:0.5 ofControl:aControl];
}

- (void)clickXPositionPercentage:(float)xPosition yPositionPercentage:(float)yPosition ofControl:(CPControl)aControl
{
    var middle = CGPointMake([aControl frame].origin.x + [aControl frame].size.width*xPosition, 
            [aControl frame].origin.y + [aControl frame].size.height*yPosition);
    
    [aControl mouseDown:[CPEvent mouseEventWithType:CPLeftMouseDown location:middle
        modifierFlags:0 timestamp:nil windowNumber:nil context:nil eventNumber:nil clickCount:nil pressure:nil]];
    [aControl mouseDown:[CPEvent mouseEventWithType:CPLeftMouseUp location:middle
        modifierFlags:0 timestamp:nil windowNumber:nil context:nil eventNumber:nil clickCount:nil pressure:nil]];
}

@end

@implementation UITestCase (Assertions)

- (void)assertNotNull:(id)object
{
    [self assertNotNull:object message:@"<"+[object class]+"> was null"];
}

- (void)assertNotNull:(id)object message:(CPString)message
{
    if(object === null || typeof(object) === undefined)
    {
        [self fail:message];
    }
}

- (void)assertNull:(id)object
{
    [self assertNull:object message:@"Object was not null!"];
}

- (void)assertNull:(id)object message:(CPString)message
{
    if(object !== null)
    {
        [self fail:message];
    }
}

- (void)assert:(id)expected equals:(id)actual
{
    [self assert:expected equals:actual message:@"Not equal! Expected<"+[expected description]+"> Got<"+[actual description]+">"];
}

- (void)assert:(id)expected equals:(id)actual message:(CPString)aMessage
{
    if(expected !== actual && ![expected isEqual:actual])
    {
        [self fail:aMessage];
    }
}

- (void)assert:(CPView)aView isPositioned:(unsigned)positionMask relativeTo:(CPView)anotherView withTolerance:(float)tolerance
{
    if(tolerance < 0)
    {
        print("Warning: Negative tolerance is not allowed and will give false positives!");
    }
    
    if(positionMask & UIViewHorizontalCenterAlignment)
    {
        var anotherViewHorizontalCenter = [anotherView center].x;
        var aViewHorizontalCenter = [aView center].x;
        
        if(ABS(anotherViewHorizontalCenter - aViewHorizontalCenter) > tolerance)
        {
            [self fail:@"The horizontal positioning of <"+[aView description]+"> did not fall within the tolerance of " + tolerance + " in respect to <"+[anotherView description]+">"];
        }
    }
    
    if(positionMask & UIViewHorizontalLeftAlignment)
    {
        var xPositionOfAnotherView = [anotherView frameOrigin].x;
        var xPositionOfAView = [aView frameOrigin].x;
        
        if(ABS(xPositionOfAView - xPositionOfAnotherView) > tolerance)
        {
            [self fail:@"The horizontal positioning of <"+[aView description]+"> did not fall within the tolerance of " + tolerance + " in respect to <"+[anotherView description]+">"];
        }
    }
}

- (void)fail:(CPString)message
{
    throw message;
}

@end
