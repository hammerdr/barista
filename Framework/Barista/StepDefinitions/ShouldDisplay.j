@import "Then.j"

var shouldDisplayRegex = /(.*) should display/;

@implementation ShouldDisplay : Then

- (BOOL)matches:(CPString)input
{
	return input.match(shouldDisplayRegex);
}

- (void)parse:(CPString)lineData context:(CPDictionary)context delegate:(UITestScript)delegate
{
	var tag = lineData.match(shouldDisplayRegex)[1];
	var view = [self findViewByTag:tag];
	if(view)
	{
		[self succeed:delegate];
	}
	else
	{
		[self failure:delegate];
	}
}

@end
