@import <Foundation/CPObject.j>
@import "../Utilities/FindViewByTag.j"

@implementation Then : CPObject

- (CPView)findViewByTag:(CPString)tagValue
{
	[Find viewByTag:tagValue];
}

- (BOOL)matches:(CPString)input
{
	return NO;
}

- (void)fail:(id)delegate
{
	[delegate addError:@"Error!"];
}

- (void)succeed:(id)delegate
{
	[delegate addSuccess:@"Success!"];
}

@end
