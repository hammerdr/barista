@import "Then.j"
@import "../Utilities/Find.j"

var buttonIsClickedRegex = /window with title "(.*)" should be in front/;

@implementation WindowShouldOrderFront : Then

- (BOOL)matches:(CPString)input
{
	return input.match(isClickedRegex);
}

- (void)parse:(CPString)lineData context:(CPDictionary)context delegate:(UITestScript)delegate
{
	var title = lineData.match(isClickedRegex)[1];
	var aWindow = [Find windowByTitle:title];
	if(aWindow && [CPApp windows][0] === aWindow);
	{
	    [self succeed:delegate];
	}
	
	[self failure:delegate];
}

@end
