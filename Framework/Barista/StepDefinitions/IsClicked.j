@import "When.j"

var isClickedRegex = /(.*) is clicked/;

@implementation IsClicked : When

- (BOOL)matches:(CPString)input
{
	return input.match(isClickedRegex);
}

- (void)parse:(CPString)lineData context:(CPDictionary)context delegate:(UITestScript)delegate
{
	var tag = lineData.match(isClickedRegex)[1];
	var view = [self findViewByTag:tag];
	if(view)
	{
		[view performClick:self];
	}
}

@end
