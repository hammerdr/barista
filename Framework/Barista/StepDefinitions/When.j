@import <Foundation/CPObject.j>
@import "../Utilities/FindViewByTag.j"

@implementation When : CPObject

-(CPView)findViewByTag:(CPString)tagValue
{
	[Find viewByTag:tagValue];
}

- (BOOL)matches:(CPString)input
{
	return NO;
}

- (Function)parse:(CPString)lineData context:(CPDictionary)context delegate:(UITestScript)delegate
{
	return function() {};
}

@end
