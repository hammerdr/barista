@import "IsClicked.j"
@import "ShouldDisplay.j"

GIVEN_DEFINITIONS = new Array();
WHEN_DEFINITIONS = new Array([[ButtonIsClicked alloc] init], [[IsClicked alloc] init]);
THEN_DEFINITIONS = new Array([[ShouldDisplay alloc] init]);