@import <Foundation/CPObject.j>
@import "../Utilities/FindViewByTag.j"


@implementation Given : CPObject

-(CPView)findViewByTag:(CPString)tagValue
{
	[Find viewByTag:tagValue];
}

- (BOOL)matches:(CPString)input
{
	return NO;
}

@end
