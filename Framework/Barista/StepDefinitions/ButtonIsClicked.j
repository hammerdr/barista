@import "When.j"
@import "../Utilities/Find.j"

var buttonIsClickedRegex = /button with title "(.*)" is clicked/;

@implementation ButtonIsClicked : When

- (BOOL)matches:(CPString)input
{
	return input.match(isClickedRegex);
}

- (void)parse:(CPString)lineData context:(CPDictionary)context delegate:(UITestScript)delegate
{
	var title = lineData.match(isClickedRegex)[1];
	var view = [Find buttonByTitle:title];
	if(view)
	{
		[view performClick:self];
	}
}

@end
