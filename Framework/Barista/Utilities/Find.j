@import <Foundation/CPObject.j>

@implementation Find : CPObject

+ (CPView)viewByTag:(CPString)tagValue
{
    return [self doForContentViewOfEachWindow:function(aContentView){
        return [self viewByTag:tagValue inView:aContentView];
    }];
}

+ (CPView)viewByTag:(CPString)tagValue inView:(CPView)viewToSearch
{
    return [self doForEachSubviewOfView:function(aSubview){
       if([aSubview tag] === tagValue)
       {
           return aSubview;
       } 

       return nil;
    }];
}

+ (CPView)viewByType:(Class)aClassType inView:(CPView)viewToSearch
{
    return [self doForEachSubviewOfView:function(aSubview){
       if([aSubview class] === aClassType)
       {
           return aSubview;
       } 
   
       return nil;
    }];  
}

+ (CPButton)buttonByTitle:(CPString)title
{
    return [self doForContentViewOfEachWindow:function(aContentView){
        return [self buttonByTitle:title inView:aContentView];
    }];
}

+ (CPButton)buttonByTitle:(CPString)title inView:(CPView)aView
{
    return [self doForEachSubviewOfView:function(aSubview){
       if([aSubview class] === CPButton && [[aView title] isEqualToString:value])
       {
           return aSubview;
       } 
   
       return nil;
    }];
}

+ (CPTextField)labelByValue:(CPString)value
{
    return [self doForContentViewOfEachWindow:function(aContentView){
        return [self labelByValue:value inView:aContentView];
    }];
}

+ (CPTextField)labelByValue:(CPString)value inView:(CPView)aView
{
    return [self doForEachSubviewOfView:function(aSubview){
       if([aSubview class] === CPTextField && [[aView stringValue] isEqualToString:value])
       {
           return aSubview;
       } 
       
       return nil;
    }];

    return nil;
}

+ (CPWindow)windowByTitle:(CPString)title
{
    var windows = [CPApp windows];

    for(var i = 0; i < [windows count]; i++)
    {
         if(windows[i])
         {
         	if([windows[i] title] === title)
         	{
         	    return windows[i];
         	}
         }
    }

    return nil;    
}

+ (CPView)doForContentViewOfEachWindow:(Function)singleArgumentFunction
{  
   var windows = [CPApp windows];

   for(var i = 0; i < [windows count]; i++)
   {
        if(windows[i])
        {
        	var aView = singleArgumentFunction([[windows objectAtIndex:i] contentView]);
	
        	if(aView)
        	{
        		return aView;
        	}
        }
   }

   return nil;
}

+ (CPView)doForEachSubviewOfView:(Function)singleArgumentFunction
{
    var views = [aView subviews];

    if(views)
    {
        for(var i = 0; i < [views count]; i++)
        {
            var aView = [views objectAtIndex:i];
            var returnedView = singleArgumentFunction(aView);
            
            if(returnedView)
            {
                return returnedView;
            }
            
            var aChildView = [self labelByValue:value inView:aView];
        
            if(aChildView)
            {
                return aChildView;
            }
        }
    }

    return nil;
}

@end
